/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2017  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NEURALBF_RUNNER_WRITER_HPP
#define NEURALBF_RUNNER_WRITER_HPP

#include "Neurons.hpp"
#include "Program.hpp"

#include <cstddef>
#include <iostream>
#include <map>
#include <set>
#include <string>

namespace neuralbf
{
namespace runner
{

/**
 * Writer outputs a network or program in a text representation.  See Reader.hpp
 * for a description of the format.  This can be used for saving compiled
 * networks, and also for debugging the compiler.
 */
class Writer
{

public:

  /**
   * Construct a writer for the given output stream.
   */
  explicit inline Writer (std::ostream& stream)
    : out(stream), neuronMap()
  {}

  Writer () = delete;
  Writer (const Writer&) = delete;
  Writer& operator= (const Writer&) = delete;

  /**
   * Write out the given network.  This resets the internal state (neuron map
   * and ID counter).
   */
  void writeNetwork (const Network& net);

  /**
   * Write out an IO group.  Must be called after writing the corresponding
   * network, so that the network's node IDs are still known.
   */
  void writeIOGroup (const IOGroup& io);

  /**
   * Write out an already defined neuron (its ID).  This is useful for writing
   * the stop neuron of a program.
   */
  void writeNeuron (const Neuron& n);

private:

  /** The output stream we write to.  */
  std::ostream& out;

  /**
   * Map neuron pointers to the ID strings used in the network serialisation.
   */
  std::map<const Neuron*, std::string> neuronMap;

  /**
   * Set of all string IDs that have been assigned to neurons already in the
   * current writing.  This is mainly useful to catch the case that a network
   * has two neurons with the same explicitly set ID.
   */
  std::set<std::string> assignedIds;

  /** Counter for next ID value, so we get unique IDs.  */
  size_t nextId;

  /**
   * Retrieve an ID for the given Neuron pointer.  This assigns a new one
   * if the neuron is not already known in neuronMap, or retrieves the value
   * from there if it is already in it.
   */
  std::string getNeuronId (const Neuron* n);

};

} // namespace runner
} // namespace neuralbf

#endif // header guard
