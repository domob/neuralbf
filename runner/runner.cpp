/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2016-2017  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Neurons.hpp"
#include "Program.hpp"
#include "Reader.hpp"

#include <cstdlib>
#include <exception>
#include <fstream>
#include <iostream>
#include <stdexcept>

using namespace neuralbf::runner;

int
main (int argc, char** argv)
{
  if (argc != 2)
    {
      std::cerr << "USAGE: runner PROGRAM-FILE" << std::endl;
      return EXIT_FAILURE;
    }
  const std::string filename = argv[1];

  try
    {
      std::ifstream in(filename);
      if (!in)
        throw std::runtime_error ("failed to open input file");

      Network net;
      Program p(net);

      Reader r(in);
      r.readNetwork (net);

      IOGroup input;
      p.addInput (input);
      r.readIOGroup (input);

      IOGroup output;
      p.addOutput (output);
      r.readIOGroup (output);

      p.setStopNeuron (&r.readNeuron ());
      in.close ();

      p.execute (std::cin, std::cout);
    }
  catch (const std::exception& exc)
    {
      std::cerr << "Error: " << exc.what () << std::endl;
      return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}
