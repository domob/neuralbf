/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2016  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Neurons.hpp"

#include <cassert>
#include <cstdlib>

using namespace neuralbf::runner;

int
main ()
{
  /* We connect two neurons in a loop and check that activation forwarding
     works as expected.  We also connect a single neuron to itself in a way
     that will oscillate.  */

  Network net;

  Neuron& a = net.addNew (1);
  Neuron& b = net.addNew (2);
  a.linkTo (2, b);
  b.linkTo (1, a);

  Neuron& c = net.addNew (0);
  Neuron& d = net.addNew (10);
  c.linkTo (-1, c);
  c.linkTo (5, d);

  a.setActive (true);

  for (unsigned i = 0; i < 10; ++i)
    {
      assert (static_cast<bool> (a) == (i % 2 == 0));     
      assert (static_cast<bool> (b) == (i % 2 == 1));     
      assert (static_cast<bool> (c) == (i % 2 == 1));
      assert (!d);

      net.step ();
    }

  return EXIT_SUCCESS;
}
