/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2017  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Neurons.hpp"
#include "Program.hpp"
#include "Reader.hpp"
#include "Writer.hpp"

#include <cstdlib>
#include <exception>
#include <fstream>
#include <iostream>
#include <stdexcept>

using namespace neuralbf::runner;

int
main (int argc, char** argv)
{
  if (argc != 3)
    {
      std::cerr << "USAGE: ioRoundtrip IN-FILE OUT-FILE" << std::endl;
      return EXIT_FAILURE;
    }
  const std::string inFile = argv[1];
  const std::string outFile = argv[2];

  try
    {
      std::ifstream in(inFile);
      if (!in)
        throw std::runtime_error ("failed to open input file");

      Network net;
      Program p(net);

      Reader r(in);
      r.readNetwork (net);

      IOGroup input;
      p.addInput (input);
      r.readIOGroup (input);

      IOGroup output;
      p.addOutput (output);
      r.readIOGroup (output);

      p.setStopNeuron (&r.readNeuron ());
      in.close ();

      std::ofstream out(outFile);
      if (!out)
        throw std::runtime_error ("failed to open output file");

      Writer w(out);
      w.writeNetwork (net);
      out << std::endl;
      w.writeIOGroup (input);
      w.writeIOGroup (output);
      w.writeNeuron (*p.getStopNeuron ());
      out.close ();
    }
  catch (const std::exception& exc)
    {
      std::cerr << "Error: " << exc.what () << std::endl;
      return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}
