/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2016  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Neurons.hpp"
#include "Program.hpp"

#include <cassert>
#include <cinttypes>
#include <cstdlib>

using namespace neuralbf::runner;

constexpr unsigned NUM_BITS = 16;

int
main ()
{
  /* In this test, we build a network that can move inputs to outputs.
     Data bits are simply connected.  The input control activates itself
     in each iteration (just zero threshold) and the output control neuron gets
     activated at each iteration after the second.  */

  Network net;
  Program p(net);

  Neuron& inControl = net.addNew (0);
  IOGroup input(inControl);
  p.addInput (input);

  Neuron& outControl = net.addNew (1);
  IOGroup output(outControl);
  p.addOutput (output);

  Neuron& waiter = net.addNew (0);
  waiter.linkTo (1, outControl);

  for (unsigned i = 0; i < NUM_BITS; ++i)
    {
      Neuron& inBit = net.addNew (1);
      input.addBit (inBit);

      Neuron& outBit = net.addNew (1);
      output.addBit (outBit);

      inBit.linkTo (1, outBit);
    }

  const std::vector<uint64_t> inputValues = {1, 42, 1000, 65535, 0, 7};
  std::vector<uint64_t> outputValues;

  auto nextInput = inputValues.begin ();
  while (true)
    {
      /* Make sure to fetch provided output before checking for more input.
         We want the output even if no more input data is there.  */
      if (output.hasOutput ())
        outputValues.push_back (output.fetchOutputNumber ());
      if (input.expectsInput ())
        {
          if (nextInput == inputValues.end ())
            break;
          input.provideInputNumber (*nextInput);
          ++nextInput;
        }
      assert (p.step ());
    }

  assert (outputValues.size () == inputValues.size ());
  for (unsigned i = 0; i < inputValues.size (); ++i)
    assert (outputValues[i] == inputValues[i]);

  return EXIT_SUCCESS;
}
