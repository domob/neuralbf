/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2016  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Neurons.hpp"
#include "Program.hpp"

#include <cassert>
#include <cinttypes>
#include <cstdlib>
#include <vector>

using namespace neuralbf::runner;

int
main ()
{
  Network net;

  Neuron& a = net.addNew (1);
  Neuron& b = net.addNew (2);
  Neuron& c = net.addNew (3);

  NeuronGroup g({&a, &b, &c});

  assert (g.numBits () == 3);

  g.set ({false, false, true});
  assert (!a && !b && c);
  assert (g.getNumber () == 4);

  g.setNumber (6);
  assert (!a && b && c);
  const std::vector<bool> values = g.get ();
  assert (values.size () == 3);
  assert (!values[0] && values[1] && values[2]);

  return EXIT_SUCCESS;
}
