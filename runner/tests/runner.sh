#!/bin/sh -e

# neuralbf, a compiler for Brainfuck to R-ANNs.
# Copyright (C) 2016-2017  Daniel Kraft <d@domob.eu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Simple test that the stopping neuron works.
../neuralrunner "${srcdir}/stopping.txt"

# More complex test of IO roundtrip for the networks and that IO works.

file1=$(mktemp)
file2=$(mktemp)
rtProgram=$(mktemp)

./ioRoundtrip "${srcdir}/copy.txt" ${rtProgram}

dd if=/dev/urandom of=${file1} bs=1K count=1
cat ${file1} | ../neuralrunner ${rtProgram} >${file2}

cmp ${file1} ${file2}
res=$?

rm -f ${file1} ${file2} ${rtProgram}
exit $res
