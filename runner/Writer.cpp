/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2017  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Writer.hpp"

#include <iostream>
#include <sstream>

namespace neuralbf
{
namespace runner
{

void
Writer::writeNetwork (const Network& net)
{
  nextId = 1;
  neuronMap.clear ();
  assignedIds.clear ();

  out << net.neurons.size() << std::endl;

  /* Neurons might be seen first either when writing them out themselves
     (from the loop over net.neurons), or when referenced by a neuron from
     this loop.  We must be able to properly assign (and retrieve an already
     assigned) ID in both cases!  */

  for (const auto& n : net.neurons)
    {
      out << std::endl;
      out << getNeuronId (n.get ()) << " " << n->threshold
          << " " << n->outputs.size () << std::endl;
      for (const auto& l : n->outputs)
        out << " " << l.weight << " " << getNeuronId (l.target) << std::endl;
    }
}

void
Writer::writeIOGroup (const IOGroup& io)
{
  writeNeuron (*io.control);
  out << " " << io.bits.bits.size ();

  for (const auto* b : io.bits.bits)
    {
      out << " ";
      writeNeuron (*b);
    }

  out << std::endl;
}

void
Writer::writeNeuron (const Neuron& n)
{
  auto mi = neuronMap.find (&n);
  if (mi == neuronMap.end ())
    throw std::runtime_error ("neuron for writing is unknown");
  out << mi->second;
}

std::string
Writer::getNeuronId (const Neuron* n)
{
  const auto mi = neuronMap.find (n);
  if (mi != neuronMap.end ())
    return mi->second;

  std::string newId;
  if (n->hasId ())
    newId = n->getId ();
  else
    {
      std::ostringstream id;
      id << "UnnamedNeuron_" << nextId;
      ++nextId;
      newId = id.str ();
    }

  if (assignedIds.count (newId) > 0)
    {
      std::ostringstream msg;
      msg << "duplicate neuron ID: " << newId;
      throw std::runtime_error (msg.str ());
    }

  neuronMap.insert (std::make_pair (n, newId));
  assignedIds.insert (newId);

  return newId;
}

} // namespace runner
} // namespace neuralbf
