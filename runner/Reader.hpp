/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2016-2017  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NEURALBF_RUNNER_READER_HPP
#define NEURALBF_RUNNER_READER_HPP

#include "Neurons.hpp"
#include "Program.hpp"

#include <iostream>
#include <map>
#include <string>

namespace neuralbf
{
namespace runner
{

/**
 * Reader is able to deserialise a network or program.  The main challenge
 * in doing that is keeping track of the mapping between neuron pointers
 * and the IDs these neurons have in the serialised form.  This state also needs
 * to be preserved for reading the IOGroup's of a program.
 *
 * Serialised form of a network:
 *
 *   <number of neurons>
 *
 *   <ID of a neuron> <threshold> <number of links>
 *   <weight> <ID of target neuron>
 *   
 *   ...
 *
 * (Neuron IDs are strings of non-whitespace characters.)
 *
 * Serialised form of an IOGroup for a program (in addition):
 *
 *   <ID of control neuron> <number of data bits of input group>
 *   <ID of data bit> ...
 *
 * The stop neuron is just given by its ID.
 */
class Reader
{

public:

  /**
   * Construct a reader for the given input stream.
   */
  explicit inline Reader (std::istream& stream)
    : in(stream), neuronMap()
  {}

  Reader () = delete;
  Reader (const Reader&) = delete;
  Reader& operator= (const Reader&) = delete;

  /**
   * Read into a given network.
   */
  void readNetwork (Network& net);

  /**
   * Read into an IO group.  Must be called after reading a network, so that
   * the network's node IDs are still known.
   */
  void readIOGroup (IOGroup& io);

  /**
   * Reads a neuron ID.  The ID must be defined already and known, and the
   * corresponding neuron is returned.  This can be used to read the stop
   * neuron of a program.
   */
  Neuron& readNeuron ();

private:

  /** The input stream we read from.  */
  std::istream& in;

  /**
   * Map neuron IDs to the actual neurons.  This is used internally while
   * reading a network, and to persist state until we read the IOGroup's of
   * a program.
   */
  std::map<std::string, Neuron*> neuronMap;

};

} // namespace runner
} // namespace neuralbf

#endif // header guard
