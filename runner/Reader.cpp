/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2016-2017  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Reader.hpp"

#include <cassert>
#include <cstddef>
#include <set>
#include <sstream>
#include <stdexcept>
#include <utility>

namespace neuralbf
{
namespace runner
{

void
Reader::readNetwork (Network& net)
{
  net.neurons.clear ();
  neuronMap.clear ();

  /* It must be possible to refer to neuron IDs before they are defined,
     so that cycles of links can be described (or even self-links).  If a ID
     is referred to in a link but not yet defined, we create the neuron
     nevertheless with a fake threshold and mark it as "ghost".  Later, when
     the neuron is properly defined, we abuse our friend status with Neuron
     to sneak in the correct threshold.  In the end, no ghosts must remain.  */
  std::set<std::string> ghosts;

  size_t numNeurons;
  in >> numNeurons;
  net.neurons.reserve (numNeurons);
  for (size_t i = 0; i < numNeurons; ++i)
    {
      std::string id;
      Scalar threshold;
      size_t numLinks;
      in >> id >> threshold >> numLinks;

      const auto mi = neuronMap.find (id);
      Neuron* n = nullptr;
      if (mi != neuronMap.end ())
        {
          const auto gi = ghosts.find (id);
          assert (gi != ghosts.end ());
          ghosts.erase (gi);
          n = mi->second;
          n->threshold = threshold;
        }
      else
        {
          n = &net.addNew (threshold);
          n->setId (id);
          neuronMap.insert (std::make_pair (id, n));
        }
      assert (n);

      for (size_t j = 0; j < numLinks; ++j)
        {
          Scalar weight;
          in >> weight >> id;

          const auto mi = neuronMap.find (id);
          if (mi != neuronMap.end ())
            n->linkTo (weight, *mi->second);
          else
            {
              Neuron& target = net.addNew (0);
              n->linkTo (weight, target);
              target.setId (id);
              neuronMap.insert (std::make_pair (id, &target));
              ghosts.insert (id);
            }
        }
    }

  if (!ghosts.empty ())
    {
      std::ostringstream msg;
      msg << "neuron ID '" << *ghosts.begin () << "' is never resolved";
      throw std::runtime_error (msg.str ());
    }
}

void
Reader::readIOGroup (IOGroup& io)
{
  io.control = &readNeuron ();
  size_t numBits;
  in >> numBits;

  io.bits.bits.clear ();
  for (size_t i = 0; i < numBits; ++i)
    io.addBit (readNeuron ());
}

Neuron&
Reader::readNeuron ()
{
  std::string id;
  in >> id;

  auto mi = neuronMap.find (id);
  if (mi == neuronMap.end ())
    {
      std::ostringstream msg;
      msg << "neuron '" << id << "' is not defined";
      throw std::runtime_error (msg.str ());
    }

  return *mi->second;
}

} // namespace runner
} // namespace neuralbf
