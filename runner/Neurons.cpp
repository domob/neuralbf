/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2016-2107  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Neurons.hpp"

namespace neuralbf
{
namespace runner
{

void
Link::fire () const
{
  if (target)
    target->receiveInput (weight);
}

void
Neuron::linkTo (Scalar weight, Neuron& target)
{
  outputs.push_back (Link (weight, target));
}

bool
Neuron::finishRound ()
{
  active = (received >= threshold);
  received = 0;
  return active;
}

void
Neuron::fire () const
{
  assert (active);
  for (const auto& o : outputs)
    o.fire ();
}

void
Neuron::setActive (bool val)
{
  active = val;
  network.markNeuronActive (*this, active);
}

Neuron&
Network::addNew (Scalar threshold)
{
  std::unique_ptr<Neuron> neuron(new Neuron (*this, threshold));
  neurons.push_back (std::move (neuron));
  return *neurons.back ();
}

void
Network::step ()
{
  for (auto* neuron : active)
    neuron->fire ();

  active.clear ();
  for (auto& neuron : neurons)
    if (neuron->finishRound ())
      active.insert (neuron.get ());
}

void
Network::logActive (std::ostream& out, const std::string& indent) const
{
  for (const auto* neuron : active)
    if (neuron->hasId ())
      out << indent << neuron->getId () << std::endl;
    else
      out << indent << "<no id>" << std::endl;
}

void
Network::markNeuronActive (Neuron& neuron, bool val)
{
  auto pos = active.find (&neuron);
  if (val)
    {
      if (pos == active.end ())
        active.insert (&neuron);
    }
  else
    {
      if (pos != active.end ())
        active.erase (pos);
    }
}

} // namespace runner
} // namespace neuralbf
