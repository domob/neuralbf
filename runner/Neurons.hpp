/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2016-2017  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NEURALBF_RUNNER_NEURONS_HPP
#define NEURALBF_RUNNER_NEURONS_HPP

#include <cassert>
#include <cinttypes>
#include <iostream>
#include <memory>
#include <set>
#include <string>
#include <vector>

namespace neuralbf
{
namespace runner
{

class Network;
class Neuron;

/** The type used for all computations.  */
using Scalar = int64_t;

/**
 * A link in the network, which corresponds to a (directed) connection
 * from a neuron to another one with weight.
 */
class Link
{

public:

  inline Link ()
    : target(nullptr)
  {}

  inline Link (Scalar w, Neuron& t)
    : weight(w), target(&t)
  {}

  Link (const Link&) = default;
  Link& operator= (const Link&) = default;

  /**
   * Fire this link.  Sends the given weight to the target neuron.
   */
  void fire () const;

private:

  /** The weight of this link.  */
  Scalar weight;

  /** The target neuron.  */
  Neuron* target;

  friend class Writer;

};

/**
 * One neuron of the executed network.
 */
class Neuron
{

public:

  /**
   * Construct the neuron with given threshold and no links so far.
   * Each neuron is bound to a particular network, which is passed to
   * the constructor.
   */
  Neuron (Network& net, Scalar t)
    : network(net), threshold(t), outputs(), active(false), received(0)
  {}

  Neuron () = delete;
  Neuron (const Neuron&) = delete;
  Neuron& operator= (const Neuron&) = delete;

  /**
   * Construct a link from this neuron to the given target.
   */
  void linkTo (Scalar weight, Neuron& target);

  /**
   * Set the string ID.
   */
  inline void
  setId (const std::string& newId)
  {
    assert (!newId.empty ());
    id = newId;
  }

  /**
   * Returns if this neuron has a set ID.
   */
  inline bool
  hasId () const
  {
    return !id.empty ();
  }

  /**
   * Returns the string ID of this neuron.  Must only be called if the
   * neuron actually has an ID.
   */
  inline const std::string&
  getId () const
  {
    assert (hasId ());
    return id;
  }

  /**
   * Receive input stimulation (from a link pointing to us).
   */
  inline void receiveInput (Scalar weight)
  {
    received += weight;
  }

  /**
   * Finish the current round.  This resets the received stimulation,
   * and evaluates activation (or not).
   * @return True if the neuron is (now) active.
   */
  bool finishRound ();

  /**
   * Fire signals to all target neurons.  The neuron must be active when this
   * is called.
   */
  void fire () const;

  /**
   * Return whether the neuron is currently active.
   */
  inline operator bool () const
  {
    return active;
  }

  /**
   * Set activation status.
   */
  void setActive (bool val);

private:

  /** The network we belong to.  */
  Network& network;

  /**
   * The activation threshold of this neuron (shift of the Heavyside
   * activation function).
   */
  Scalar threshold;

  /** Output-links to other neurons.  */
  std::vector<Link> outputs;

  /**
   * String ID of the neuron.  If this is not empty, it is used for
   * serialisation of the network.  In this case, the ID must be unique among
   * all neurons of the network that have an ID.
   */
  std::string id;

  /** Whether or not this neuron is currently active.  */
  bool active;

  /** The current signals received in this round.  */
  Scalar received;

  friend class Reader;
  friend class Writer;

};

/**
 * A full neural network, which consists of many (connected) neurons.
 */
class Network
{

public:

  /**
   * Create an empty network.
   */
  inline Network ()
    : neurons(), active()
  {}

  Network (const Network&) = delete;
  Network& operator= (const Network&) = delete;

  /**
   * Construct a neuron and add it to the network.
   * @param threshold The activation theshold for the neuron.
   * @return A reference to the created neuron.
   */
  Neuron& addNew (Scalar threshold);

  /**
   * Perform a full step of the network, firing and updating all neurons.
   */
  void step ();

  /**
   * Print log of currently active neurons to the given stream.  This is useful
   * for debugging purposes.
   */
  void logActive (std::ostream& out, const std::string& indent) const;

private:

  /** All neurons in the network.  */
  std::vector<std::unique_ptr<Neuron>> neurons;

  /**
   * Pointer to currently active neurons.  This is used as optimisation, so that
   * we only have to fire those neurons that are actually active.
   */
  std::set<Neuron*> active;

  /**
   * Mark the given neuron as active (or not).  This method must be used
   * when manually activating neurons.
   */
  void markNeuronActive (Neuron& neuron, bool val);

  friend class Neuron;
  friend class Reader;
  friend class Writer;

};

} // namespace runner
} // namespace neuralbf

#endif // header guard
