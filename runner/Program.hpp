/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2016-2017  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NEURALBF_RUNNER_PROGRAM_HPP
#define NEURALBF_RUNNER_PROGRAM_HPP

#include "Neurons.hpp"

#include <cassert>
#include <cinttypes>
#include <cstddef>
#include <iostream>
#include <utility>
#include <vector>

namespace neuralbf
{
namespace runner
{

/**
 * A group of neurons that are collectively used to represent a "word"
 * for IO or other purposes.
 */
class NeuronGroup
{

public:

  /**
   * Create an empty group.  Neurons can be added later one by one.
   */
  inline NeuronGroup ()
    : bits()
  {}

  /**
   * Create with the given neurons.
   */
  explicit inline NeuronGroup (std::vector<Neuron*>&& b)
    : bits(b)
  {}

  NeuronGroup (const NeuronGroup&) = default;
  NeuronGroup& operator= (const NeuronGroup&) = delete;

  /**
   * Add a data neuron.
   */
  inline void
  addBit (Neuron& n)
  {
    bits.push_back (&n);
  }

  /**
   * Get number of bits declared.
   */
  inline size_t
  numBits () const
  {
    return bits.size ();
  }

  /**
   * Get the neuron corresponding to the given bit.  The index must be in the
   * range 0 ... numBits-1, with 0 corresponding to the least significant bit.
   */
  Neuron& getNeuron (size_t ind);

  /**
   * Set data to the given bits.
   */
  void set (const std::vector<bool>& data);

  /**
   * Set data from the bits of the given integer.  The bits are encoded
   * in little endian into the neuron vector.
   */
  void setNumber (uint64_t data);

  /**
   * Get the data encoded as bits.
   */
  std::vector<bool> get () const;

  /**
   * Get data as an integer.  The neurons are interpreted as little endian
   * bits for the resulting number.
   */
  uint64_t getNumber () const;

private:

  /** The data neurons.  */
  std::vector<Neuron*> bits;

  friend class Reader;
  friend class Writer;

};

/**
 * A group of n+1 neurons as used for input/output.
 */
class IOGroup
{

public:

  /**
   * Construct the IOGroup without setting a control neuron.  This is used
   * to add the IOs before deserialising their data.  Other uses will leave
   * the control neuron uninitialised and crash the program.
   */
  inline IOGroup ()
    : control(nullptr), bits()
  {}

  /**
   * Construct the IO group with just the control for now and no data bits.
   * They can be added later one by one.
   */
  explicit inline IOGroup (Neuron& c)
    : control(&c), bits()
  {}

  /**
   * Construct the IO group from the given neurons.
   */
  inline IOGroup (Neuron& c, std::vector<Neuron*>&& b)
    : control(&c), bits(std::move(b))
  {}

  /**
   * Construct the IO group from the given NeuronGroup.
   */
  inline IOGroup (Neuron& c, NeuronGroup& g)
    : control(&c), bits(g)
  {}

  IOGroup (const IOGroup&) = delete;
  IOGroup& operator= (const IOGroup&) = delete;

  /**
   * Add a data neuron.
   */
  inline void
  addBit (Neuron& n)
  {
    bits.addBit (n);
  }

  /**
   * Get number of bits declared.
   */
  inline size_t
  numBits () const
  {
    return bits.numBits ();
  }

  /**
   * Check whether we expect input.
   */
  inline bool
  expectsInput () const
  {
    assert (control);
    return *control;
  }

  /**
   * Check whether we have output.
   */
  inline bool
  hasOutput () const
  {
    assert (control);
    return *control;
  }

  /**
   * Provide input with the given bits.
   */
  void provideInput (const std::vector<bool>& data);

  /**
   * Provide input from the bits of the given integer.  The bits are encoded
   * in little endian into the neuron vector.
   */
  void provideInputNumber (uint64_t data);

  /**
   * Accept output to the given bits.
   */
  std::vector<bool> fetchOutput ();

  /**
   * Accept output as an integer.  The neurons are interpreted as little endian
   * bits for the resulting number.
   */
  uint64_t fetchOutputNumber ();

private:

  /** The control neuron.  */
  Neuron* control;

  /** All data neurons.  */
  NeuronGroup bits;

  friend class Reader;
  friend class Writer;

};

/**
 * A "program" represented by a R-ANN (Network).  This class handles the
 * "ABI" for input and output.  This class owns no subobjects, but rather
 * aggregates them loosely.  It is therefore a thin and flexible layer over
 * the network.
 */
class Program
{

public:

  /**
   * Construct the program with the given underlying network.
   */
  explicit inline Program (Network& n)
    : net(n), stop(nullptr)
  {}

  Program () = delete;
  Program (const Program&) = delete;
  Program& operator= (const Program&) = delete;

  /**
   * Add a new input group.
   */
  inline void
  addInput (IOGroup& g)
  {
    inputs.push_back (&g);
  }

  /**
   * Add a new output group.
   */
  inline void
  addOutput (IOGroup& g)
  {
    outputs.push_back (&g);
  }

  /**
   * Set the stop neuron (might be null for not using one).  This just stores
   * a reference to it for the program ABI, adding the neuron to the network
   * is the responsibility of the caller.
   */
  inline void
  setStopNeuron (Neuron* n)
  {
    stop = n;
  }

  /**
   * Get the stop neuron.  Used for writing of programs.
   */
  inline const Neuron*
  getStopNeuron () const
  {
    return stop;
  }

  /**
   * Try to step the underlying network.  This only works if no IO action
   * is pending.
   * @return True if the step was performed.
   */
  bool step ();

  /**
   * Execute the program with input/output bound to the given streams.  This
   * expects that we have exactly one 8-bit input and output.
   */
  void execute (std::istream& in, std::ostream& out);

private:

  /** The underlying network.  */
  Network& net;

  /** All inputs of the program.  */
  std::vector<IOGroup*> inputs;
  /** All outputs of the program.  */
  std::vector<IOGroup*> outputs;

  /** The stop neuron of the network (if any).  */
  Neuron* stop;

};

} // namespace runner
} // namespace neuralbf

#endif // header guard
