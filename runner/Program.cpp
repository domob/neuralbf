/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2016-2017  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Program.hpp"

#include <stdexcept>

namespace neuralbf
{
namespace runner
{


namespace
{

constexpr bool verboseExecution = false;

void
executionLog (const std::string& str)
{
  if (verboseExecution)
    std::cerr << str << std::endl;
}

} // anonymous namespace

Neuron&
NeuronGroup::getNeuron (size_t ind)
{
  if (ind >= numBits ())
    throw std::runtime_error ("index of neuron out of bounds");
  return *bits[ind];
}

void
NeuronGroup::set (const std::vector<bool>& data)
{
  if (data.size () != bits.size ())
    throw std::runtime_error ("mismatch between input and number of bits");
  for (unsigned i = 0; i < bits.size (); ++i)
    bits[i]->setActive (data[i]);
}

void
NeuronGroup::setNumber (uint64_t data)
{
  std::vector<bool> dataBits(bits.size (), false);
  assert (dataBits.size () == bits.size ());

  unsigned ind = 0;
  while (data)
    {
      if (ind >= dataBits.size ())
        throw std::runtime_error ("input integer has too many bits set");

      dataBits[ind] = (data & 1);
      data >>= 1;
      ++ind;
    }

  set (dataBits);
}

std::vector<bool>
NeuronGroup::get () const
{
  std::vector<bool> result;
  result.reserve (bits.size ());
  for (const auto* n : bits)
    result.push_back (*n);

  return result;
}

uint64_t
NeuronGroup::getNumber () const
{
  if (bits.size () > 64)
    throw std::runtime_error ("too many output bits, cannot encode in number");

  uint64_t res = 0;
  uint64_t mask = 1;
  for (const auto* n : bits)
    {
      if (*n)
        res |= mask;
      mask <<= 1;
    }

  return res;
}

void
IOGroup::provideInput (const std::vector<bool>& data)
{
  assert (control);

  if (!expectsInput ())
    throw std::runtime_error ("input provided but not expected");

  control->setActive (false);
  bits.set (data);

  assert (!expectsInput ());
}

void
IOGroup::provideInputNumber (uint64_t data)
{
  assert (control);

  if (!expectsInput ())
    throw std::runtime_error ("input provided but not expected");

  control->setActive (false);
  bits.setNumber (data);

  assert (!expectsInput ());
}

std::vector<bool>
IOGroup::fetchOutput ()
{
  assert (control);

  if (!hasOutput ())
    throw std::runtime_error ("output requested but not available");
  control->setActive (false);
  assert (!hasOutput ());

  return bits.get ();
}

uint64_t
IOGroup::fetchOutputNumber ()
{
  assert (control);

  if (!hasOutput ())
    throw std::runtime_error ("output requested but not available");
  control->setActive (false);
  assert (!hasOutput ());

  return bits.getNumber ();
}

bool
Program::step ()
{
  for (const auto* in : inputs)
    if (in->expectsInput ())
      return false;
  for (const auto* out : outputs)
    if (out->hasOutput ())
      return false;

  if (verboseExecution)
    {
      std::cerr << "  Before stepping network active:" << std::endl;
      net.logActive (std::cerr, "    ");
    }
  net.step ();
  if (verboseExecution)
    {
      std::cerr << "  After stepping network active:" << std::endl;
      net.logActive (std::cerr, "    ");
    }

  return true;
}

void
Program::execute (std::istream& in, std::ostream& out)
{
  if (inputs.size () != 1 || outputs.size () != 1)
    throw std::runtime_error ("need exactly one in and output");
  IOGroup& input = *inputs[0];
  IOGroup& output = *outputs[0];

  if (input.numBits () != 8 || output.numBits () != 8)
    throw std::runtime_error ("input and output must have 8 bits");

  while (true)
    {
      executionLog ("Starting execution loop iteration.");

      /* Make sure to fetch potential outputs before asking for more input.
         Waiting for input may end the program if EOF is hit.  */
      if (output.hasOutput ())
        {
          out << static_cast<char> (output.fetchOutputNumber ());
          out.flush ();
          executionLog ("  Fetched output number.");
        }

      if (stop && *stop)
        {
          executionLog ("  Stop neuron activated.");
          break;
        }

      if (input.expectsInput ())
        {
          const int c = in.get ();
          if (!in)
            {
              if (in.eof ())
                {
                  executionLog ("  No more input, stopping.");
                  break;
                }
              throw std::runtime_error ("failed to read input");
            }
          input.provideInputNumber (static_cast<uint64_t> (c));
          executionLog ("  Provided input number.");
        }
      if (!step ())
        throw std::runtime_error ("stepping the program failed");
    }
}

} // namespace runner
} // namespace neuralbf
