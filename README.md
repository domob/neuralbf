# NeuralBF

`neuralbf` is a proof-of-concept compiler for the
[Brainfuck](https://en.wikipedia.org/wiki/Brainfuck)
"programming language" to [recurrent artificial neural networks
(R-ANNs)](https://en.wikipedia.org/wiki/Recurrent_neural_network).

We exclusively use neurons with a (shifted) Heavyside activation function.
Each neuron can have multiple inputs (with given positive and negative
weights) and a single output whose value is normalised to one.  All numbers
are integers.

This project consists of two parts, the **runner** and the **compiler**.

## Runner

The R-ANNs produced can be executed by the runner tool.  For this, we define
a **simple ABI** that maps standard input/output of a process (the running
program) to the activation of nodes in the network.

### Input

Input for the R-ANN is handled by `n+1` neurons, where `n` is the number of
bits per input character (typically eight).  When the R-ANN is ready to accept
another character from input, the additional *control neuron* is
activated.  Then the runtime environment atomically deactivates the control
neuron and sets the data neurons according to the input bits.

### Output

Output is also handled by `n+1` neurons, in a similar fashion with `n` data
neurons for the bits of the character and a control neuron.  Whenever the
R-ANN wants to output a character, it sets the data bits accordingly and
activates the control neuron.  The runtime environment sets the control
neuron to inactive when the character has been received.

### Program End

In addition, the program also defines a special *stop neuron*.  When it gets
activated, then the execution is terminated immediately by the runtime.  This
also happens when input is requested but EOF received.

## Compiler

The compiler is the tool that actually interprets a Brainfuck program and
translates it into the corresponding network structure.  The resulting network
can then be executed by the runner to perform the Brainfuck instructions.

## Construction of Networks

### Program Flow

The **program flow** (which instruction of the compiled/interpreted Brainfuck
program is active at the moment) is represented by a string of *flow neurons*.
Each such neuron corresponds (roughly) to a specific instruction of the
program, and the neuron's activation signals that this instruction corresponds
to the currently active one in the present program execution.  The neurons are
simply connected to each other, so that each one activates the next one
(except for special handling of program flow like the
[`[]` instructions](#loops)).
The last instruction of the program is connected to the stop neuron, so that
the execution halts after finishing the Brainfuck code.

For triggering the initial flow, we need a neuron that fires exactly once
at the beginning of the program.  This can be achieved by having two
neurons `A` and `I`:  `A` has a threshold of zero and is connected to `I`, while
`I` is connected to itself and negatively to `A`.  Then `A` activates initially
and can be used to trigger program flow, but as soon as I becomes active
starting from the second iteration onwards, it will inhibit `A` so that
the trigger is only produced once.  We also connect `A` to itself negatively,
so that `A` is deactivated already for the second step (when `I` is just
becoming active for the first time).

### Memory Tape <a name="tape"></a>

A critical component of Brainfuck (and a Turing machine in general) is the
**memory tape** that stores the program's state.  As in the Turing model,
Brainfuck assumes that we have an infinite tape with a currently-active
position and the ability to shift it left or right.  Basic storage in the
neural network is implemented by *neurons that are connected back to
themselves*.  Each one corresponds to a bit, and if no external input is
provided, it just keeps its state.  Such neurons build the actual data bits
for all of the memory tape.

There are multiple possible designs for how shifting the tape can be
implemented.  We decided to literally shift the tape:  In other words, always
the same neurons represent the bits of the currently active cell.  This
simplifies implementation of operations like IO and increment/decrement, since
they work on the same neurons.  The main complexity with this design is that
we *need a way to shift data* in the cells left or right.  Let us now describe
how a shift to the left works; shifting right is done analogously.

To indicate that a **shift to the left** is desired, let us assume that a
particular neuron is activated.  In practice, this could be a dedicated
control neuron, but it is more efficient to simply interpret each flow neuron
corresponding to a shift instruction as the control.  To implement the shift
atomically, we make use of a separate set of neurons for each data bit being
shifted.  Connections of the neurons are done as follows, where `S` is the data
neuron of a bit being shifted, `D` is the data neuron of the same bit in the
cell to the left (where we shift to) and `T` is the temporary storage neuron for
the shift.  `C` is the neuron indicating the shift, which is the same neuron for
all data bits:

1. `S` and `C` are linked to `T`, such that `T` is activated only if both are
    active.
2. `C` is linked with a strongly negative signal to `S` (and, since the same
   applies to all data neurons, also `D`).
3. `T` is linked to `D`.

1) and 2) ensure that once `C` is activated (to start the shift), the data bit
will "move" from `S` to `T`, leaving `S` inactive in any case afterwards.
Due to 3), the bit will be moved to `D` (which has been turned inactive
similarly to `S`) in the following step.

The whole process therefore takes *two iterations*, which means that we have to
add an *additional flow neuron* between the shift activation and the flow neuron
corresponding to the following instruction.

### Shift Operations

The shift operations themselves (`<>` in Brainfuck) are easy to implement
based on the described model for our [memory tape](#tape):
For each shift operation, we construct a control neuron for the tape and
connect the temporary flow neuron (see above) to the flow neuron of the
following instruction.

### IO Operations

IO is represented by `.` and `,` in Brainfuck.  We designate the tape's head
neurons as data neurons both for input and for output.  We then connect the
previous instruction's flow output to both the control neuron for input
and output, respectively, and a flow neuron corresponding to the IO
instruction.

Note that it would be possible to make the IO "zero cost" by connecting
the activation input directly to the input of the following instruction
(without using a designated flow neuron for the instruction itself).  This,
however, breaks if multiple IO operations follow each other immediately.
Thus we cannot do that.

### Conditional Looping <a name="loops"></a>

Looping and conditionals are handled by `[]` in Brainfuck.  For `[`, we add
two temporary flow neurons (`Z` and `N`).  `Z` is connected to the previous
instruction's flow output and also connected, with negative weights, to
all head data neurons.  *It is only activated if the head byte is zero.*
We connect `N` in a similar way so that it is *only activated if the head byte
is non-zero* (and the previous instruction is finished).  For that, we connect
all head neurons to `N` (with a small positive weight one) and also connect
the previous instruction's flow output to `N` with a high weight `W`.
(`W` needs to be at least as large as the number of data bits.)
If we then set the activation threshold for `N` to `W+1`, we have ensured that
`N` is only activated if both the previous neuron and at least one of the
data bits are active.

`N` is then used as trigger for the instruction following `[`, and `Z` is used
as trigger for the instruction following `]`.  Flow inputs into the looping
instruction can come from both the instruction before `[`
and the one before `]`.  In this way, the looping construct `[]` is handled
"as a whole" at once, with no clear separation of `[` and `]`.

### Arithmetic

Finally, Brainfuck supports basic arithmetic through the `+` and `-` operations.
They simply increment or decrement the number at the current tape head.
We implement them by adding an *arithmetic unit* to the tape that allows
**incrementing or decrementing the tape head**, and then connecting the trigger
signals for each `+` or `-` operation to this unit.  The actual neurons that
implement the operation are thus only present once, and each operation just
adds the neurons needed for its control flow.  This is similar to how tape
shifts work.

Let us now describe *how the increment and decrement operations work*.  We
assume that they are to be performed *in-place* on `N` data bits, which are
connected as in the tape already to keep their values if no external influence
is there.

#### Increment

The increment operation works in two steps.  In the first step, we find the
*least-significant zero* present in the input data, assuming it to be at
position `N+1` if all `N` bits are ones.  Then, we *flip that zero to a one and
all less significant ones to zeros*, keeping all more significant bits as
they are originally.

In the network, we then need `N+1` temporary neurons `Z[1] ... Z[N+1]`, where
exactly one will be activated at the position of the least significant zero
in the input.  Activation of each of those neurons must be made conditional
on the trigger neuron `T` for the operation, and it must be connected to all
less significant data bits in a way that it gets activated only if all of
them are ones.  Furthermore, it must not get activated if the data bit at
the same position is a one as well.  All together, `Z[i]` needs to be set up
like this:

* The operation trigger neuron `T` is connected to `Z[i]` with weight `1`.
* The data bits `D[j]` with `j < i` are connected to `Z[i]` with weight `1`.
* The data bit `D[i]` is connected to `Z[i]` with weight `-1`.
* `Z[i]` has its threshold set to `i`.

In the next step, we need to use the activation of one of the `Z` neurons to
update the data bits themselves.  For this, we connect each `Z` neuron with
negative weights to all less significant data bits, so that they get zero'ed
out, and connect it to the data bit at the same level to turn it into a one.

#### Decrement

Decrementing a number works similarly to incrementing (as described above).
We have to *find the least-significant one* present in the input.  Then, *all
bits up to this position will be flipped* (turning the one to a zero and
all lesser-significant zeros to ones).

As for the increment, we need `N+1` temporary neurons `Z[1] ... Z[N+1]`.  When
the operation is performed, exactly one will be activated at the position of
the least significant one (or at position `N+1` if all data bits are zeros).
These are the connections that we need:

* The operation trigger neuron `T` is connected to `Z[i]` with weight `1`.
* The data bit `D[i]` is connected to `Z[i]` with weight `1`.
* The data bits `D[j]` with `j < i` are connected to `Z[i]` with weight `-1`.
* The threshold of `Z[i]` is set to `2`.  As a special case, for `i = N+1`,
  the threshold is `1`.

For the next step, we connect `Z[i]` to all `D[j]` with `j < i` with a positive
weight, so that all less-significant zeros are changed to ones.  Similarly,
we connect `Z[i]` to `D[i]` with a negative weight, to flip the least
significant one in the input to a zero.

Both operations take *two iterations in total*, so that we have to connect
the operation trigger `T` to an *intermediary flow neuron* that then forwards
activation to the flow neuron that acts as trigger for the next instruction.
(Similar to shifts of the [tape](#tape).)
