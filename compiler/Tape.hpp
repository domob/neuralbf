/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2016-2017  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NEURALBF_COMPILER_TAPE_HPP
#define NEURALBF_COMPILER_TAPE_HPP

#include "Neurons.hpp"
#include "Program.hpp"

#include <cstddef>
#include <string>
#include <vector>

namespace neuralbf
{
namespace compiler
{

/**
 * Class to build the network representation of the memory tape.  It builds
 * all the storage neurons and implements the logic to shift the tape left
 * and right.  It also allows "direct" access to the neurons at the head.
 */
class Tape
{

public:

  /**
   * Construct a new tape based on the given network and with the specified
   * dimensions.  It won't yet have any control neurons for the shifts, those
   * need to be added later as necessary.  The tape length is added to both
   * sides of the head position.
   */
  Tape (runner::Network& n, size_t numBits, size_t len);

  Tape () = delete;
  Tape (const Tape&) = delete;
  Tape& operator= (const Tape&) = delete;

  /**
   * Give access to the group of neurons at the head.
   */
  inline runner::NeuronGroup&
  getHead ()
  {
    return headGroup;
  }
  inline const runner::NeuronGroup&
  getHead () const
  {
    return headGroup;
  }

  /**
   * Tell the tape about a neuron that should act (when firing) as trigger
   * for shifting left.  Returned is a neuron that will fire when the shift
   * is completed.
   */
  inline runner::Neuron&
  createLeftShift (const std::string& prefix, runner::Neuron& trigger)
  {
    return createShift (prefix, trigger, leftNeurons);
  }

  /**
   * Add a trigger for shifting right.
   */
  inline runner::Neuron&
  createRightShift (const std::string& prefix, runner::Neuron& trigger)
  {
    return createShift (prefix, trigger, rightNeurons);
  }

private:

  /** The network in which we build the neurons.  */
  runner::Network& net;

  /** The neuron group at the "head" position.  */
  runner::NeuronGroup headGroup;

  /** All data neurons (needed to connect control neurons).  */
  std::vector<runner::Neuron*> dataNeurons;

  /** All left-shift temporary neurons.  */
  std::vector<runner::Neuron*> leftNeurons;
  /** All right-shift temporary neurons.  */
  std::vector<runner::Neuron*> rightNeurons;

  /**
   * Internal helper function for createLeftShift / createRightShift.
   */
  runner::Neuron& createShift (const std::string& prefix,
                               runner::Neuron& trigger,
                               std::vector<runner::Neuron*>& temps);

};

} // namespace compiler
} // namespace neuralbf

#endif // header guard
