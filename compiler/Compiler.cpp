/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2016-2017  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Compiler.hpp"

#include "Instructions.hpp"

#include "Writer.hpp"

#include <cstddef>
#include <sstream>
#include <stack>
#include <stdexcept>

constexpr size_t NUM_BITS = 8;
constexpr size_t TAPE_LEN = 100;

using namespace neuralbf::runner;

namespace neuralbf
{
namespace compiler
{

Compiler::Compiler ()
  : net(), program(net),
    tape(net, NUM_BITS, TAPE_LEN), arith(net, tape.getHead ()),
    inControl(net.addNew (1)), outControl(net.addNew (1)),
    input(inControl, tape.getHead ()), output(outControl, tape.getHead ()),
    stop(net.addNew (1)),
    hasProgram(false)
{
  inControl.setId ("InputControl");
  outControl.setId ("OutputControl");
  stop.setId ("Stop");

  program.addInput (input);
  program.addOutput (output);
  program.setStopNeuron (&stop);
}

void
Compiler::compile (std::istream& input)
{
  assert (!hasProgram);
  hasProgram = true;

  StartupTrigger startup(net);
  Neuron* trigger = &startup.getTrigger ();
  size_t pos = 1;

  /* We keep track of all open loops in a stack.  When we find [, we create
     a new one and push it onto the stack.  If we find the matching ], we pop
     the top element from the stack and use its LoopClose as instruction.  */
  std::stack<std::unique_ptr<ConditionalLoop>> loops;

  while (true)
    {
      const char c = input.get ();
      if (!input)
        {
          if (input.eof ())
            break;
          throw std::runtime_error ("failed to read input program");
        }

      std::ostringstream instPos;
      instPos << "_Pos_" << pos << "_";

      /* To manage the instruction object, we store it into the inst unique_ptr.
         For loops, however, the object will actually be owned by the loops
         stack.  Thus, we need to separate ownership from the pointer that
         is used later to connect the current instruction.  */
      std::unique_ptr<Instruction> inst;
      Instruction* currentInst = nullptr;

      switch (c)
        {
        case '<':
          inst.reset (new LeftShift ("LeftShift" + instPos.str (), net, tape));
          break;
        case '>':
          inst.reset (new RightShift ("RightShift" + instPos.str (),
                                      net, tape));
          break;

        case '+':
          inst.reset (new Increment ("Increment" + instPos.str (), net, arith));
          break;
        case '-':
          inst.reset (new Decrement ("Decrement" + instPos.str (), net, arith));
          break;

        case '.':
          inst.reset (new InputOrOutput ("Output" + instPos.str (),
                                         net, tape.getHead (), outControl));
          break;
        case ',':
          inst.reset (new InputOrOutput ("Input" + instPos.str (),
                                         net, tape.getHead (), inControl));
          break;

        case '[':
          loops.emplace (std::make_unique<ConditionalLoop> (
              "Loop" + instPos.str (), net, tape.getHead ()));
          currentInst = loops.top ().get ();
          break;

        case ']':
          if (loops.empty ())
            throw std::runtime_error ("mismatched []");
          currentInst = &loops.top ()->getLoopClose ();
          inst = std::move (loops.top ());
          loops.pop ();
          break;

        default:
          // This is a "comment", just ignore.
          break;
        }

      // In the default case, connect the currentInst pointer to inst.
      if (inst && !currentInst)
        currentInst = inst.get ();

      if (currentInst)
        {
          currentInst->setActivationInput (*trigger);
          trigger = &currentInst->getActivationOutput ();
          ++pos;
        }
    }

    // If there are open loops left, this is an error.
    if (!loops.empty ())
      throw std::runtime_error ("mismatched []");

    // Finish up by linking the final trigger to the stop neuron.
    trigger->linkTo (1, stop);
}

void
Compiler::writeProgram (std::ostream& out) const
{
  assert (hasProgram);

  Writer w(out);
  w.writeNetwork (net);
  out << std::endl;
  w.writeIOGroup (input);
  w.writeIOGroup (output);
  out << std::endl;
  w.writeNeuron (stop);
}

} // namespace compiler
} // namespace neuralbf
