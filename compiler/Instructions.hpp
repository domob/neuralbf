/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2016-2017  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NEURALBF_COMPILER_INSTRUCTIONS_HPP
#define NEURALBF_COMPILER_INSTRUCTIONS_HPP

#include "Arithmetic.hpp"
#include "Tape.hpp"

#include "Neurons.hpp"
#include "Program.hpp"

#include <string>

namespace neuralbf
{
namespace compiler
{

/**
 * StartupTrigger constructs the necessary neurons to trigger the program
 * execution initially at startup.
 */
class StartupTrigger
{

public:

  /**
   * Construct the startup trigger.  This sets up the neurons already.
   */
  explicit StartupTrigger (runner::Network& net);

  StartupTrigger () = delete;
  StartupTrigger (const StartupTrigger&) = delete;
  StartupTrigger& operator= (const StartupTrigger&) = delete;

  inline runner::Neuron&
  getTrigger ()
  {
    return *activator;
  }

private:

  /** The activation neuron that triggers execution.  */
  runner::Neuron* activator;

};

/**
 * Base class for an "instruction".  Instructions correspond to one of the
 * eight characters in Brainfuck.  They get activation input from some external
 * neuron and provide their own neuron that activates following instructions.
 */
class Instruction
{

public:

  virtual ~Instruction ();

  Instruction () = delete;
  Instruction (const Instruction&) = delete;
  Instruction& operator= (const Instruction&) = delete;

  virtual void setActivationInput (runner::Neuron& trigger) = 0;
  virtual runner::Neuron& getActivationOutput () = 0;

protected:

  inline Instruction (const std::string& p, runner::Network& n)
    : net(n), prefix(p)
  {}

  /** The network reference, can be used by subclasses.  */
  runner::Network& net;

  /** The prefix for neuron IDs.  */
  const std::string prefix;

};

/**
 * An instruction that is based on "external logic" and just needs to connect
 * the Instruction interface to it.  This is the common base used for shift
 * operations (with their main logic defined by the tape) and arithmetic
 * operations (with their logic from Arithmetic).
 */
class ExternalLogicInstruction : public Instruction
{

public:

  void setActivationInput (runner::Neuron& trigger) override;
  runner::Neuron& getActivationOutput () override;

protected:

  inline ExternalLogicInstruction (const std::string& p, runner::Network& n)
    : Instruction(p, n), externalDone(nullptr)
  {}

  /**
   * Provides the link to the external implementation, needs to be defined
   * by concrete subclasses.  This function is given an initial trigger neuron
   * and must return a neuron that can be used to then trigger the following
   * instruction.
   */
  virtual runner::Neuron& setUpExternal (runner::Neuron& trigger) = 0;

private:

  /** The activation output neuron (provided by external implementation).  */
  runner::Neuron* externalDone;

};

/**
 * A tape-based external instruction (shifts).
 */
class TapeInstruction : public ExternalLogicInstruction
{

protected:

  inline TapeInstruction (const std::string& p, runner::Network& n, Tape& t)
    : ExternalLogicInstruction(p, n), tape(t)
  {}

  /** The underlying Tape object.  */
  Tape& tape;

};

/**
 * A left shift instruction, based on the tape.
 */
class LeftShift : public TapeInstruction
{

public:

  inline LeftShift (const std::string& p, runner::Network& n, Tape& t)
    : TapeInstruction(p, n, t)
  {}

protected:

  runner::Neuron& setUpExternal (runner::Neuron& trigger) override;

};

/**
 * A right shift instruction, based on the tape.
 */
class RightShift : public TapeInstruction
{

public:

  inline RightShift (const std::string& p, runner::Network& n, Tape& t)
    : TapeInstruction(p, n, t)
  {}

protected:

  runner::Neuron& setUpExternal (runner::Neuron& trigger) override;

};

/**
 * An arithmetic instruction (increment or decrement).
 */
class ArithmeticInstruction : public ExternalLogicInstruction
{

protected:

  inline ArithmeticInstruction (const std::string& p, runner::Network& n,
                                Arithmetic& a)
    : ExternalLogicInstruction(p, n), arith(a)
  {}

  /** The underlying Arithmetic object.  */
  Arithmetic& arith;

};

/**
 * An increment operation.
 */
class Increment : public ArithmeticInstruction
{

public:

  inline Increment (const std::string& p, runner::Network& n, Arithmetic& a)
    : ArithmeticInstruction(p, n, a)
  {}

protected:

  runner::Neuron& setUpExternal (runner::Neuron& trigger) override;

};

/**
 * A decrement operation.
 */
class Decrement : public ArithmeticInstruction
{

public:

  inline Decrement (const std::string& p, runner::Network& n, Arithmetic& a)
    : ArithmeticInstruction(p, n, a)
  {}

protected:

  runner::Neuron& setUpExternal (runner::Neuron& trigger) override;

};

/**
 * An IO operation (. or ,).  They both look alike, the only difference is which
 * control neuron they get passed in the constructor.
 */
class InputOrOutput : public Instruction
{

public:

  inline InputOrOutput (const std::string& p, runner::Network& n,
                        runner::NeuronGroup& d, runner::Neuron& c)
    : Instruction(p, n), data(d), control(c), flow(nullptr)
  {}

  void setActivationInput (runner::Neuron& trigger) override;
  runner::Neuron& getActivationOutput () override;

private:

  /** The NeuronGroup that is read/written.  */
  runner::NeuronGroup& data;

  /** The control neuron.  */
  runner::Neuron& control;

  /** The flow neuron corresponding to this instruction.  */
  runner::Neuron* flow;

};

/**
 * A conditional loop instruction ([] in Brainfuck).  It is special among
 * all instructions, because both [ and ] are handled as a whole in the network.
 * This pair is represented by a ConditionalLoop object.  It also implements
 * the instruction interface, which corresponds to the [ part.  In addition,
 * we provide access to a second instruction object, which corresponds to ].
 * In this way, the compiler can connect the control flow for both [ and ]
 * correctly.
 */
class ConditionalLoop : public Instruction
{

public:

  ConditionalLoop (const std::string& p, runner::Network& n,
                   runner::NeuronGroup& d);

  void setActivationInput (runner::Neuron& trigger) override;
  runner::Neuron& getActivationOutput () override;

  /**
   * Access the loop close instruction.
   */
  inline Instruction&
  getLoopClose ()
  {
    return loopClose;
  }

private:

  /**
   * The Instruction class that represents the closing ], based on this
   * conditional loop.
   */
  class LoopClose : public Instruction
  {

  public:

    inline LoopClose (const std::string& p, runner::Network& n,
                      ConditionalLoop& l)
      : Instruction(p, n), loop(l)
    {}

    void setActivationInput (runner::Neuron& trigger) override;
    runner::Neuron& getActivationOutput () override;

  private:

    /** The ConditionalLoop this corresponds to.  */
    ConditionalLoop& loop;

  };


  /** The NeuronGroup that controls the condition.  */
  runner::NeuronGroup& data;

  /** The flow neuron for data being zero.  */
  runner::Neuron* flowZero;
  /** The flow neuron for data being non-zero.  */
  runner::Neuron* flowNonZero;

  /** The LoopClose instruction for this [] pair.  */
  LoopClose loopClose;

};

} // namespace compiler
} // namespace neuralbf

#endif // header guard
