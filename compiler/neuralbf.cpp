/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2016-2017  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Compiler.hpp"

#include <cstdlib>
#include <exception>
#include <fstream>
#include <iostream>
#include <stdexcept>

using namespace neuralbf::compiler;
using namespace neuralbf::runner;

int
main (int argc, char** argv)
{
  if (argc < 2 || argc > 3)
    {
      std::cerr << "USAGE: neuralbf PROGRAM-FILE [OUTPUT-FILE]" << std::endl;
      return EXIT_FAILURE;
    }
  const std::string programFile = argv[1];
  std::string outputFile;
  if (argc >= 3)
    outputFile = argv[2];

  try
    {
      std::ifstream in(programFile);
      if (!in)
        throw std::runtime_error ("failed to open input file");

      Compiler comp;
      comp.compile (in);
      in.close ();

      Program& p = comp.getProgram ();

      if (!outputFile.empty ())
        {
          std::ofstream out(outputFile);
          if (!out)
            throw std::runtime_error ("failed to open output file");

          comp.writeProgram (out);
          out.close ();
        }
      else
        {
          std::cerr << "Compilation done, ready for input." << std::endl;
          p.execute (std::cin, std::cout);
        }
    }
  catch (const std::exception& exc)
    {
      std::cerr << "Error: " << exc.what () << std::endl;
      return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}
