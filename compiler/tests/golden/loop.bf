Reset read cell to zero and then hardcode an A with 65 increments
,[+]
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Read into a cell and then move the value to the two neighbouring cells
.
,[->+>+<<]>.>.

Increment a cell until it overflows which should give us exactly 255 outputs
,>[-]+[+<.>]

Copy newline at end of file
,.
