/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2017  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Arithmetic.hpp"
#include "Tape.hpp"

#include "Neurons.hpp"
#include "Program.hpp"

#include <cassert>
#include <cinttypes>
#include <cstddef>

using namespace neuralbf::compiler;
using namespace neuralbf::runner;

constexpr size_t BITS = 8;
constexpr uint64_t MOD = 1 << BITS;

int
main ()
{
  Network net;
  Tape tape(net, BITS, 0);  /* We only need the head group of the tape.  */
  NeuronGroup& group = tape.getHead ();
  Arithmetic arith(net, group);

  Neuron& startInc = net.addNew (1);
  Neuron& incDone = arith.createIncrement ("", startInc);
  Neuron& startDec = net.addNew (1);
  Neuron& decDone = arith.createDecrement ("", startDec);

  for (uint64_t num = 0; num < MOD; ++num)
    {
      group.setNumber (num);
      startInc.setActive (true);
      incDone.setActive (false);
      while (!incDone)
        net.step ();
      assert (group.getNumber () == (num + 1) % MOD);

      group.setNumber (num);
      startDec.setActive (true);
      decDone.setActive (false);
      while (!decDone)
        net.step ();
      assert (group.getNumber () == (num + MOD - 1) % MOD);
    }

  return EXIT_SUCCESS;
}
