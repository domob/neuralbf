#!/bin/sh -e

# neuralbf, a compiler for Brainfuck to R-ANNs.
# Copyright (C) 2016-2017  Daniel Kraft <d@domob.eu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# "End-to-end" test for the neuralbf compiler, which compiles and executes
# given code with given input and then compares the expected output.

cases="empty basicIo tapeShift arithmetic loop"

out=$(mktemp)

for c in ${cases}
do
  echo "Running $c..."
  ../neuralbf "${srcdir}/golden/${c}.bf" <"${srcdir}/golden/${c}.in" >${out}
  cmp ${out} "${srcdir}/golden/${c}.out"
done

rm -f ${out}
