/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2016-2017  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Tape.hpp"

#include "Neurons.hpp"
#include "Program.hpp"

#include <cassert>
#include <cinttypes>
#include <cstddef>
#include <cstdlib>

using namespace neuralbf::compiler;
using namespace neuralbf::runner;

constexpr size_t BITS = 8;
constexpr size_t LEN = 10;

/**
 * Utility class for testing Tape operations.  It constructs a tape and the
 * necessary neurons so that we can manually shift the tape and verify
 * the head contents.
 */
class TapeTester
{

public:

  TapeTester ();

  TapeTester (const TapeTester&) = delete;
  TapeTester& operator= (const TapeTester&) = delete;

  inline void
  shiftLeft ()
  {
    shiftInternal (startLeftShift);
  }
  inline void
  shiftRight ()
  {
    shiftInternal (startRightShift);
  }

  inline void
  checkNumber (const uint64_t expected) const
  {
    assert (expected == tape.getHead ().getNumber ());
  }

  inline void
  setNumber (const uint64_t num)
  {
    tape.getHead ().setNumber (num);
  }

private:

  Network net;
  Tape tape;

  Neuron& startLeftShift;
  Neuron& startRightShift;
  Neuron& shiftDone;

  /**
   * Internal worker method to implement left and right shifts.  It activates
   * the flow neuron and then steps the network until the done neuron is
   * activated.
   */
  void shiftInternal (Neuron& startShift);

};

TapeTester::TapeTester ()
  : net(), tape(net, BITS, LEN),
    startLeftShift(net.addNew (1)),
    startRightShift(net.addNew (1)),
    shiftDone(net.addNew (1))
{
  tape.createLeftShift ("", startLeftShift).linkTo (1, shiftDone);
  tape.createRightShift ("", startRightShift).linkTo (1, shiftDone);
}

void
TapeTester::shiftInternal (Neuron& startShift)
{
  startShift.setActive (true);
  shiftDone.setActive (false);
  while (!shiftDone)
    net.step ();
}

int
main ()
{
  TapeTester tester;

  tester.setNumber (1);
  tester.checkNumber (1);
  tester.shiftLeft ();
  tester.setNumber (2);
  tester.shiftLeft ();
  tester.setNumber (3);
  tester.shiftRight ();
  tester.checkNumber (2);
  tester.shiftRight ();
  tester.checkNumber (1);
  tester.shiftRight ();
  tester.setNumber (42);
  tester.shiftLeft ();
  tester.checkNumber (1);
  tester.shiftLeft ();
  tester.checkNumber (2);
  tester.shiftLeft ();
  tester.checkNumber (3);
  tester.shiftRight ();
  tester.checkNumber (2);
  tester.shiftRight ();
  tester.checkNumber (1);
  tester.shiftRight ();
  tester.checkNumber (42);

  return EXIT_SUCCESS;
}
