/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2017  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Instructions.hpp"
#include "Tape.hpp"

#include "Neurons.hpp"
#include "Program.hpp"

#include <cassert>
#include <cinttypes>
#include <cstddef>

using namespace neuralbf::compiler;
using namespace neuralbf::runner;

constexpr size_t BITS = 8;

int
main ()
{
  Network net;
  Tape tape(net, BITS, 0);  /* We only need the head group of the tape.  */
  NeuronGroup& group = tape.getHead ();

  // Construct two trigger neurons (for the two instructions of the loop).
  Neuron& trigger1 = net.addNew (1);
  Neuron& trigger2 = net.addNew (1);

  // Set up the conditional loop instruction.  This gives us two following
  // neurons, which should be activated in the right way depending on the
  // value of the tape head.
  ConditionalLoop loop("", net, group);
  loop.setActivationInput (trigger1);
  Neuron& ifNonZero = loop.getActivationOutput ();
  loop.getLoopClose ().setActivationInput (trigger2);
  Neuron& ifZero = loop.getLoopClose ().getActivationOutput ();

  for (uint64_t num = 0; num < (1 << BITS); ++num)
    {
      group.setNumber (num);

      // Each of the two trigger neurons should be fine.
      for (unsigned trigger = 0; trigger < 2; ++trigger)
        {
          trigger1.setActive (trigger == 0);
          trigger2.setActive (trigger != 0);
          ifNonZero.setActive (false);
          ifZero.setActive (false);

          while (!ifNonZero && !ifZero)
            net.step ();

          assert (ifNonZero == (num != 0));
          assert (ifZero == (num == 0));
        }
    }

  return EXIT_SUCCESS;
}
