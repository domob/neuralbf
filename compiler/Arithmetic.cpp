/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2017  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Arithmetic.hpp"

#include <cassert>
#include <sstream>

using namespace neuralbf::runner;

namespace neuralbf
{
namespace compiler
{

Arithmetic::Arithmetic (Network& n, NeuronGroup& group)
  : net(n), incTemp(), decTemp()
{
  incTemp.reserve (group.numBits () + 1);
  decTemp.reserve (group.numBits () + 1);
  for (size_t i = 0; i <= group.numBits (); ++i)
    {
      Neuron& incCur = net.addNew (i + 1);
      incTemp.push_back (&incCur);
      Neuron& decCur = net.addNew (i == group.numBits () ? 1 : 2);
      decTemp.push_back (&decCur);

      std::ostringstream incId;
      incId << "Increment_Temp_Bit_" << i;
      incCur.setId (incId.str ());

      std::ostringstream decId;
      decId << "Decrement_Temp_Bit_" << i;
      decCur.setId (decId.str ());

      for (size_t j = 0; j < i; ++j)
        {
          group.getNeuron (j).linkTo (1, incCur);
          group.getNeuron (j).linkTo (-1, decCur);
          incCur.linkTo (-1, group.getNeuron (j));
          decCur.linkTo (1, group.getNeuron (j));
        }

      if (i < group.numBits ())
        {
          group.getNeuron (i).linkTo (-1, incCur);
          group.getNeuron (i).linkTo (1, decCur);
          incCur.linkTo (1, group.getNeuron (i));
          decCur.linkTo (-1, group.getNeuron (i));
        }
    }
  assert (incTemp.size () == group.numBits () + 1);
  assert (decTemp.size () == group.numBits () + 1);
}

Neuron&
Arithmetic::createOp (const std::string& prefix, Neuron& trigger,
                      std::vector<runner::Neuron*>& temps)
{
  /* Link the trigger to a temporary flow neuron and the done neuron, building
     the main flow.  */
  Neuron& intermediate = net.addNew (1);
  intermediate.setId (prefix + "Intermediate");
  trigger.linkTo (1, intermediate);
  Neuron& done = net.addNew (1);
  done.setId (prefix + "Done");
  intermediate.linkTo (1, done);

  /* Link the trigger neuron to all the temporary neurons.  */
  for (auto* tmp : temps)
    trigger.linkTo (1, *tmp);

  return done;
}

} // namespace compiler
} // namespace neuralbf
