/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2016-2017  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Instructions.hpp"

#include <cassert>

using namespace neuralbf::runner;

namespace neuralbf
{
namespace compiler
{

StartupTrigger::StartupTrigger (Network& net)
{
  activator = &net.addNew (0);
  activator->setId ("StartupTrigger_Activator");
  activator->linkTo (-1, *activator);
  Neuron& inhibitor = net.addNew (1);
  inhibitor.setId ("StartupTrigger_Inhibitor");
  activator->linkTo (1, inhibitor);
  inhibitor.linkTo (1, inhibitor);
  inhibitor.linkTo (-1, *activator);
}

Instruction::~Instruction ()
{}

void
ExternalLogicInstruction::setActivationInput (Neuron& trigger)
{
  assert (!externalDone);
  externalDone = &setUpExternal (trigger);
}

Neuron&
ExternalLogicInstruction::getActivationOutput ()
{
  assert (externalDone);
  return *externalDone;
}

Neuron&
LeftShift::setUpExternal (Neuron& trigger)
{
  return tape.createLeftShift (prefix, trigger);
}

Neuron&
RightShift::setUpExternal (Neuron& trigger)
{
  return tape.createRightShift (prefix, trigger);
}

Neuron&
Increment::setUpExternal (Neuron& trigger)
{
  return arith.createIncrement (prefix, trigger);
}

Neuron&
Decrement::setUpExternal (Neuron& trigger)
{
  return arith.createDecrement (prefix, trigger);
}

void
InputOrOutput::setActivationInput (Neuron& trigger)
{
  assert (!flow);
  flow = &net.addNew (1);
  flow->setId (prefix + "Flow");
  trigger.linkTo (1, control);
  trigger.linkTo (1, *flow);
}

Neuron&
InputOrOutput::getActivationOutput ()
{
  assert (flow);
  return *flow;
}

ConditionalLoop::ConditionalLoop (const std::string& p, Network& n,
                                  NeuronGroup& d)
  : Instruction(p, n), data(d), loopClose(p, n, *this)
{
  flowZero = &net.addNew (1);
  flowZero->setId (prefix + "IfZero");
  flowNonZero = &net.addNew (data.numBits () + 1);
  flowNonZero->setId (prefix + "IfNotZero");
  for (size_t i = 0; i < data.numBits (); ++i)
    {
      data.getNeuron (i).linkTo (-1, *flowZero);
      data.getNeuron (i).linkTo (1, *flowNonZero);
    }
}

void
ConditionalLoop::setActivationInput (Neuron& trigger)
{
  trigger.linkTo (1, *flowZero);
  trigger.linkTo (data.numBits (), *flowNonZero);
}

Neuron&
ConditionalLoop::getActivationOutput ()
{
  return *flowNonZero;
}

void
ConditionalLoop::LoopClose::setActivationInput (Neuron& trigger)
{
  loop.setActivationInput (trigger);
}

Neuron&
ConditionalLoop::LoopClose::getActivationOutput ()
{
  return *loop.flowZero;
}

} // namespace compiler
} // namespace neuralbf
