/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2016-2017  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Tape.hpp"

#include <sstream>

using namespace neuralbf::runner;

namespace neuralbf
{
namespace compiler
{

Tape::Tape (Network& n, size_t numBits, size_t len)
  : net(n), headGroup(), dataNeurons(), leftNeurons(), rightNeurons()
{
  const ptrdiff_t signed_len = len;

  /* Construct the basic data neurons and the temporary neurons, connected
     already in the way we need them.  This is done per bit separately,
     and for each bit, we go through the cells, keep track of the current
     and the previous cell's data neuron, and produce the necessary
     constructions for the space inbetween.  */
  for (size_t b = 0; b < numBits; ++b)
    {
      Neuron* prev = nullptr;
      for (ptrdiff_t i = -signed_len; i <= signed_len; ++i)
        {
          std::ostringstream idPrefix;
          idPrefix << "Tape_Pos_" << i << "_Bit_" << b << "_";

          Neuron& cur = net.addNew (1);
          cur.setId (idPrefix.str () + "Data");
          cur.linkTo (1, cur);

          dataNeurons.push_back (&cur);
          if (i == 0)
            headGroup.addBit (cur);

          if (prev)
            {
              /* The temporary neuron if we shift left, which means that cur
                 will be moved into prev.  */
              Neuron& left = net.addNew (2);
              left.setId (idPrefix.str () + "LeftTemp");
              cur.linkTo (1, left);
              left.linkTo (1, *prev);
              leftNeurons.push_back (&left);

              /* The temporary neuron for a right shift, prev to cur.  */
              Neuron& right = net.addNew (2);
              right.setId (idPrefix.str () + "RightTemp");
              prev->linkTo (1, right);
              right.linkTo (1, cur);
              rightNeurons.push_back (&right);
            }

          prev = &cur;
        }
    }
}

Neuron&
Tape::createShift (const std::string& prefix,
                   Neuron& trigger, std::vector<Neuron*>& temps)
{
  /* Link the trigger to a temporary flow neuron and the done neuron, building
     the main flow.  */
  Neuron& intermediate = net.addNew (1);
  intermediate.setId (prefix + "ShiftIntermediate");
  trigger.linkTo (1, intermediate);
  Neuron& done = net.addNew (1);
  done.setId (prefix + "ShiftDone");
  intermediate.linkTo (1, done);

  /* Link the control neuron with a negative signal to all data neurons, such
     that they are cleared before setting them to the moved bits.  */
  for (auto* dn : dataNeurons)
    trigger.linkTo (-1, *dn);

  /* Link control neuron to the temporary neurons.  */
  for (auto* t : temps)
    trigger.linkTo (1, *t);

  return done;
}

} // namespace compiler
} // namespace neuralbf
