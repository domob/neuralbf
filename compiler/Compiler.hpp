/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2016-2017  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NEURALBF_COMPILER_COMPILER_HPP
#define NEURALBF_COMPILER_COMPILER_HPP

#include "Arithmetic.hpp"
#include "Tape.hpp"

#include "Neurons.hpp"
#include "Program.hpp"

#include <cassert>
#include <iostream>
#include <memory>

namespace neuralbf
{
namespace compiler
{

/**
 * Compiler is the main class.  It turns a Brainfuck program (given on an
 * input stream) into a Network and Program.
 */
class Compiler
{

public:

  /**
   * Construct the compiler.  It won't yet have a compiled network, but
   * sets up the basic infrastructure.
   */
  Compiler ();

  Compiler (const Compiler&) = delete;
  Compiler& operator= (const Compiler&) = delete;

  /**
   * Compile Brainfuck from the given input stream.
   */
  void compile (std::istream& input);

  /**
   * Write the compiled output to an ASCII file.
   */
  void writeProgram (std::ostream& output) const;

  /* Accessor methods.  */

  inline runner::Network&
  getNetwork ()
  {
    assert (hasProgram);
    return net;
  }

  inline runner::Program&
  getProgram ()
  {
    assert (hasProgram);
    return program;
  }

  inline runner::IOGroup&
  getInput ()
  {
    assert (hasProgram);
    return input;
  }

  inline runner::IOGroup&
  getOutput ()
  {
    assert (hasProgram);
    return output;
  }

private:

  /** The neural network created.  */
  runner::Network net;

  /** The full Program wrapped around the network.  */
  runner::Program program;

  /** The tape used for the compiled network.  */
  Tape tape;

  /** Arithmetic unit used for the compiled network.  */
  Arithmetic arith;

  /** The input control neuron.  */
  runner::Neuron& inControl;
  /** The output control neuron.  */
  runner::Neuron& outControl;

  /** The program's input neurons.  */
  runner::IOGroup input;
  /** The program's output neurons.  */
  runner::IOGroup output;

  /** The stop neuron.  */
  runner::Neuron& stop;

  /** Keep track of whether we already compiled a program into the network.  */
  bool hasProgram;

};

} // namespace compiler
} // namespace neuralbf

#endif // header guard
