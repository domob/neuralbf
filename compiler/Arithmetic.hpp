/*
    neuralbf, a compiler for Brainfuck to R-ANNs.
    Copyright (C) 2017  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NEURALBF_COMPILER_ARITHMETIC_HPP
#define NEURALBF_COMPILER_ARITHMETIC_HPP

#include "Neurons.hpp"
#include "Program.hpp"

namespace neuralbf
{
namespace compiler
{

/**
 * Arithmetic builds the basic infrastructure around a given NeuronGroup for
 * in-place arithmetic operations (i. e., increment and decrement) of the number
 * represented by the neuron group.
 */
class Arithmetic
{

public:

  /**
   * Constructs a new arithmetic unit based on the given NeuronGroup.  The new
   * neurons are added to n.
   */
  explicit Arithmetic (runner::Network& n, runner::NeuronGroup& group);

  Arithmetic () = delete;
  Arithmetic (const Arithmetic&) = delete;
  Arithmetic& operator= (const Arithmetic&) = delete;

  /**
   * Tells the arithmetic unit about a neuron that should act (when firing) as
   * trigger for an increment.  Returned is a neuron that will fire when the
   * operation is completed.
   */
  inline runner::Neuron&
  createIncrement (const std::string& prefix, runner::Neuron& trigger)
  {
    return createOp (prefix + "Inc", trigger, incTemp);
  }

  /**
   * Tells the arithmetic unit about a neuron that should act as trigger for
   * a decrement operation.  Returned is the neuron that signals completion.
   */
  inline runner::Neuron&
  createDecrement (const std::string& prefix, runner::Neuron& trigger)
  {
    return createOp (prefix + "Dec", trigger, decTemp);
  }

private:

  /** The network in which we build the neurons.  */
  runner::Network& net;

  /**
   * The temporary neurons for increment, which are needed to connect new
   * trigger flow neurons.
   */
  std::vector<runner::Neuron*> incTemp;

  /**
   * The temporary neurons for decrement.
   */
  std::vector<runner::Neuron*> decTemp;

  /**
   * Internal helper function for construction of an operation.
   */
  runner::Neuron& createOp (const std::string& prefix, runner::Neuron& trigger,
                            std::vector<runner::Neuron*>& temps);

};

} // namespace compiler
} // namespace neuralbf

#endif // header guard
